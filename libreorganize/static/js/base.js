$(document).scroll(function () {
    $('.navbar').toggleClass('scrolled', $(this).scrollTop() > 0);
});

$(document).ready(function () {
    setTimeout(function () {
        $('.alert-dismissible').alert('close');
    }, 3000);
});

$(function () {
    $('#open-dashboard').click(function (e) {
        document.getElementById('dashboard').classList.add('show');
        document.getElementById('dashboard').classList.remove('hide');
        $("body").css("overflow", "hidden");
    });
    $('#close-dashboard').click(function (e) {
        document.getElementById('dashboard').classList.add('hide');
        document.getElementById('dashboard').classList.remove('show');
        $("body").css("overflow", "auto");
    });
});
