from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver


class NewVisitorTest(StaticLiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(1)

    def tearDown(self):
        self.browser.quit()

    def test_register_and_login(self):
        # Heylin needs to manage the membership list of NOVALACIRO.
        # She hears there's a cool platform called LibreOrganize that
        # will do just what she needs. She goes to check out its home page.
        self.browser.get(self.live_server_url + '/')

        # She notices that the page title says LibreOrganize, so she
        # knows she's in the right place.
        assert 'LibreOrganize' in self.browser.title

        # She notices links to login and registration.
        self.browser.find_element_by_xpath('//a[@href="/accounts/login/"]').click()

        # Since this is her first time, she chooses to create an account.
        self.browser.find_element_by_xpath('//a[@href="/accounts/register/"]').click()

        # After clicking on the register link, she is presented with a
        # form showing the fields needed for an account.
        self.browser.find_element_by_name('username').send_keys('SeleniumTestUser')
        self.browser.find_element_by_name('email1').send_keys('selenium@test.user')
        self.browser.find_element_by_name('email2').send_keys('selenium@test.user')
        self.browser.find_element_by_name('password1').send_keys('STP12345')
        self.browser.find_element_by_name('password2').send_keys('STP12345')
        self.browser.find_element_by_name('first_name').send_keys('Selenium-Test')
        self.browser.find_element_by_name('last_name').send_keys('User')
        self.browser.execute_script('window.scrollTo(0, document.body.scrollHeight)')
        self.browser.find_element_by_name('captcha_1').send_keys('PASSED')
        self.browser.find_element_by_name('tos').click()
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()
        assert 'Welcome to LibreOrganize, Selenium-Test! You have successfully registered.' \
            in self.browser.find_element_by_class_name("alert-dismissible").text

        # After submitting the form, she is redirected to the home page,
        # but this time she sees her name in the upper right hand corner
        # of the screen indicating that she is logged in.
        self.browser.find_element_by_xpath('//button[@class="close"]').click()
        self.browser.find_element_by_xpath('//a[@href="/accounts/logout/"]').click()

        # Heylin wonders whether the site will remember her. She clicks the
        # logout button, and then clicks the login button to login again.
        self.browser.find_element_by_xpath('//button[@class="close"]').click()
        self.browser.find_element_by_xpath('//a[@href="/accounts/login/"]').click()

        # Her username and password are accepted, and she is logged in to the
        # site.
        self.browser.find_element_by_name('username').send_keys('SeleniumTestUser')
        self.browser.find_element_by_name('password').send_keys('STP12345')
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()
        assert 'Welcome back, Selenium-Test! You have successfully logged in.' in self.browser.find_element_by_class_name("alert-dismissible").text
