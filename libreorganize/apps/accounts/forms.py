import re

from django import forms
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from django.utils.safestring import mark_safe
from django.contrib.auth.tokens import default_token_generator
from django.template import loader
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from captcha.fields import CaptchaField
from django_countries.fields import CountryField

from captcha.fields import CaptchaTextInput
from timezone_field import TimeZoneField
import pytz
TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
#from apps.profiles.models import Profile
#from apps.accounts.models import RegistrationProfile
#from apps.accounts.utils import send_registration_activation_email
#from apps.emails.models import Email


class LoginForm(forms.Form):

    user = None

    username = forms.CharField(label=_('Username'), max_length=30)
    password = forms.CharField(label=_('Password'), widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(LoginForm, self).__init__(*args, **kwargs)

    def clean(self):
        self.cleaned_data = super(LoginForm, self).clean()
        if self._errors:
            return

        user = authenticate(request=self.request,
                            username=self.cleaned_data['username'],
                            password=self.cleaned_data['password'])

        if user:
            if user.is_active:
                self.user = user
            else:
                raise forms.ValidationError(
                    _('Your account is not activated. Check your email for an activation link.'))
        else:
            raise forms.ValidationError(
                _('The username and/or password you entered are incorrect.'))
        return self.cleaned_data

    def login(self, request):
        if self.is_valid():
            login(request, self.user)
            request.session.set_expiry(settings.LOGIN_EXPIRATION)
            return True
        return False


class RegistrationForm(forms.Form):

    username = forms.RegexField(regex=r'^\w+$', max_length=30, label=_('Username'))
    email1 = forms.EmailField(label=_('Email'))
    email2 = forms.EmailField(label=_('Verify Email'))
    password1 = forms.CharField(widget=forms.PasswordInput(render_value=False), label=_('Password'),
                                help_text=_('Must have at least 8 characters, a letter, and a number'))
    password2 = forms.CharField(widget=forms.PasswordInput(render_value=False), label=_('Verify Password'))
    title = forms.ChoiceField(choices=settings.TITLE_CHOICES, required=False, label=_('Title'))
    first_name = forms.CharField(max_length=30, label=_('First Name'))
    last_name = forms.CharField(max_length=30, label=_('Last Name'))
    date_of_birth = forms.DateField(required=False, input_formats=['%m/%d/%Y'], label=_('Date of Birth'),
                                    help_text=_('Must be formatted as MM/DD/YYYY'))
    gender = forms.ChoiceField(choices=settings.GENDER_CHOICES, required=False, label=_('Gender'))
    company = forms.CharField(max_length=50, required=False, label=_('Company / School / University'))
    phone = forms.RegexField(regex=r'^[0-9]+$', max_length=10, required=False, label=_('Phone Number'),
                             help_text=_('Must not have any spaces, paranthesis, or dashes'))
    address1 = forms.CharField(max_length=150, required=False, label=_('Address Line 1'),
                               help_text=_('Street address, P.O. box'))
    address2 = forms.CharField(max_length=150, required=False, label=_('Address Line 2'),
                               help_text=_('Apartment, suite, unit, building, floor'))
    city = forms.CharField(max_length=30, required=False, label=_('City'))
    state = forms.CharField(max_length=30, required=False, label=_('State / Region / Province'))
    zipcode = forms.CharField(max_length=10, required=False, widget=forms.TextInput(), label=_('ZIP / Postal Code'))
    country = CountryField(blank=True, blank_label='').formfield()
    time_zone = forms.ChoiceField(choices=TIMEZONES, label=_('Time Zone'))
    language = forms.ChoiceField(label=_('Language'), choices=settings.LANGUAGES)
    captcha = CaptchaField(label=_('CAPTCHA'), widget=CaptchaTextInput(),
                           help_text=_('Type the letters you see in the box above.'))
    tos = forms.BooleanField(widget=forms.CheckboxInput(), label=_('I agree to the Terms of Service'),
                             error_messages={'required': 'You must agree to the Terms of Service to proceed.'})

    def clean_username(self):
        if User.objects.filter(username__iexact=self.cleaned_data['username']).count() > 0:
            raise forms.ValidationError(_('This username is not available.'))
        return self.cleaned_data['username']

    def clean_email1(self):
        if User.objects.filter(email__iexact=self.cleaned_data['email1']):
            raise forms.ValidationError(_('This email is already in use.'))
        self.cleaned_data['email1'] = self.cleaned_data['email1'].lower()
        return self.cleaned_data['email1']

    def clean_email2(self):
        self.cleaned_data['email2'] = self.cleaned_data['email2'].lower()
        return self.cleaned_data['email2']

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        if settings.PASSWORD_REGEX:
            if not re.match(settings.PASSWORD_REGEX, password1):
                raise forms.ValidationError(mark_safe(
                    _('The password needs to have at least 8 characters, a letter, and a number.')))

        return password1

    def clean(self):
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(
                    _('The passwords do not match.'))

        if 'email1' in self.cleaned_data and 'email2' in self.cleaned_data:
            if self.cleaned_data['email1'] != self.cleaned_data['email2']:
                raise forms.ValidationError(_('The emails do not match.'))

        if self._errors:
            return

        return self.cleaned_data

    def save(self, profile_callback=None):
        """
        This is essentially a light wrapper around
        ``RegistrationProfile.objects.create_inactive_user()``,
        feeding it the form data and a profile callback(see the
        documentation on ``create_inactive_user()`` for details) if
        supplied.

        """
        new_user = User.objects.create_user(self.cleaned_data['username'],
                                            self.cleaned_data['email1'],
                                            self.cleaned_data['password1'])

        new_user.first_name = self.cleaned_data['first_name']
        new_user.last_name = self.cleaned_data['last_name']
        new_user.is_active = True
        new_user.save()
        """
        new_profile = Profile(user=new_user,
                              company=self.cleaned_data['company'],
                              phone=self.cleaned_data['phone'],
                              address=self.cleaned_data['address'],
                              city=self.cleaned_data['city'],
                              state=self.cleaned_data['state'],
                              country=self.cleaned_data['country'],
                              zipcode=self.cleaned_data['zipcode'],
                              )

        new_profile.creator = new_user
        new_profile.creator_username = new_user.username
        new_profile.owner = new_user
        new_profile.owner_username = new_user.username
        new_profile.save()
        """
        return new_user


class PasswordResetForm(forms.Form):
    email = forms.EmailField(label=_("Email"))

    def clean_email(self):
        email = self.cleaned_data["email"]
        self.email = email
        try:
            self.user_cache = User.objects.get(email=email.lower(), is_active=True)
        except User.DoesNotExist:
            raise forms.ValidationError(_('The email is not associated with any active accounts.'))
        return email

    def save(self, email_template_name='registration/password_reset_email_user_list.html', **kwargs):
        """
        Generates a one-use only link for resetting password and sends to the designated email.
        The email will contain links for resetting passwords for all accounts associated to the email.
        """
        domain_override = kwargs.get('domain_override', False)
        use_https = kwargs.get('use_https', False)
        token_generator = kwargs.get(
            'token_generator', default_token_generator)

        user_to = {'uid': urlsafe_base64_encode(force_bytes(self.user_cache.pk)),
                   'user': self.user_cache,
                   'token': token_generator.make_token(self.user_cache)}
        t = loader.get_template(email_template_name)
        c = {
            'email': self.email,
            'site_url': settings.SITE_NAME,
            'site_name': site_name,
            'user_list': user_to,
            'protocol': use_https and 'https' or 'http',
        }

        from_email = settings.DEFAULT_FROM_EMAIL
        email = Email(
            sender=from_email,
            recipient=user.email,
            subject=_("Password reset for %s") % settings.SITE_NAME,
            body=t.render(context=c))
        email.send()


class PasswordChangeForm(SetPasswordForm):
    def __init__(self, *args, **kwargs):
        super(PasswordChangeForm, self).__init__(*args, **kwargs)

        self.fields['new_password1'].widget = forms.PasswordInput()
        self.fields['new_password2'].widget = forms.PasswordInput()

    def clean_new_password1(self):
        new_password1 = self.cleaned_data.get('new_password1')
        if settings.PASSWORD_REGEX:
            if not re.match(settings.PASSWORD_REGEX, new_password1):
                raise forms.ValidationError(mark_safe(
                    'The password needs to have at least 8 characters, a letter, and a number.'))

        return new_password1
