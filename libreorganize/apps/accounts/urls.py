from django.urls import path, re_path
from django.contrib.auth.views import PasswordResetConfirmView, PasswordResetCompleteView, PasswordResetDoneView
from django.views.generic import TemplateView

from apps.accounts import views, forms

app_name = 'apps.accounts'

urlpatterns = [
    path('login/', views.login_view, name='login'),

    path('logout/', views.logout_view, name='logout'),

    path('register/', views.register_view, name='register'),

    re_path(r'^password/change/(?P<id>\d+)/$', views.password_change_view, name='change_password'),

    re_path(r'^password/change/done/(?P<id>\d+)/$', views.password_change_done_view, name='change_password_done'),

    path('password/reset/', views.password_reset_view, name='reset_password'),

    re_path(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
            PasswordResetConfirmView.as_view(template_name='registration/custom_password_reset_confirm.html', form_class=forms.PasswordChangeForm),
            name='reset_password_confirmation'),

    path('password/reset/complete/', PasswordResetCompleteView.as_view(template_name='registration/custom_password_reset_complete.html'),
         name='reset_password_complete'),

    path('password/reset/done/', PasswordResetDoneView.as_view(template_name='registration/custom_password_reset_done.html'),
         name='reset_password_done'),

    path('register/complete/', TemplateView.as_view(template_name='accounts/registration_complete.html'),
         name='register_complete'),
]
