from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.views import PasswordResetView
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.decorators import login_required

from apps.accounts.forms import LoginForm, RegistrationForm
from apps.accounts.forms import PasswordResetForm, PasswordChangeForm
from core.decorators import ssl_required


@ssl_required
def login_view(request, form_class=LoginForm, template_name="login.html"):

    redirect_to = request.GET.get('next', '')

    if request.method == "POST":

        # Make sure redirect_to isn't garabage
        if not redirect_to or "://" in redirect_to or " " in redirect_to:
            redirect_to = reverse('home')  # PROFILE

        form = form_class(request.POST, request=request)
        if form.login(request):
            messages.add_message(request, messages.SUCCESS,
                                 _('Welcome back, %(FIRST_NAME)s! You have successfully logged in.') %
                                 {'FIRST_NAME': request.user.first_name})
            return HttpResponseRedirect(redirect_to)
    else:
        form = form_class(request=request)

        if request.user.is_authenticated and redirect_to:
            return HttpResponseRedirect(redirect_to)
        elif request.user.is_authenticated:
            messages.add_message(request, messages.WARNING,
                                 _('You are already logged in as %(FIRST_NAME)s! (%(EMAIL)s)') %
                                 {'FIRST_NAME': request.user.first_name, 'EMAIL': request.user.email})
            return HttpResponseRedirect(reverse('home'))  # PROFILE

    return render(request=request, template_name=template_name, context={"form": form})


@ssl_required
def logout_view(request, template_name='logout.html'):
    if request.user.is_authenticated:
        messages.add_message(request, messages.SUCCESS,
                             _('You have successfully logged out.'))
    else:
        messages.add_message(request, messages.WARNING,
                             _('You are already logged out.'))
    logout(request)
    return HttpResponseRedirect(reverse('home'))


@ssl_required
def register_view(request, form_class=RegistrationForm, template_name='register.html'):

    form_params = {}
    if request.session.get('form_params', None):
        form_params = request.session.pop('form_params')

    if request.method == 'POST':
        form = form_class(data=request.POST, **form_params)
        if form.is_valid():
            new_user = form.save()
            messages.add_message(request, messages.SUCCESS, _("Welcome to %(SITE_NAME)s, %(FIRST_NAME)s! You have successfully registered.") %
                                 {'SITE_NAME': settings.SITE_NAME, 'FIRST_NAME': form.cleaned_data['first_name']})
            new_user = authenticate(request=request, username=form.cleaned_data['username'], password=form.cleaned_data['password1'])
            login(request, new_user)
            request.session.set_expiry(settings.LOGIN_EXPIRATION)
            return HttpResponseRedirect(reverse('home'))
    else:
        request.session['form_params'] = form_params
        form = form_class(**form_params)

    if request.user.is_authenticated:
        messages.add_message(request, messages.WARNING, _("You are already logged in as %(FIRST_NAME)s! You cannot register an account while being logged in.") %
                             {'FIRST_NAME': request.user.first_name})
        return HttpResponseRedirect(reverse('home'))

    return render(request=request, template_name=template_name, context={'form': form})


def password_reset_view(request):
    auth_password_reset = PasswordResetView.as_view(form_class=PasswordResetForm,
                                                    template_name='password_reset.html',
                                                    email_template_name='registration/password_reset_email_user_list.html')
    return auth_password_reset(request)


@ssl_required
@login_required
def password_change_view(request, id, template_name='password_change.html', password_change_form=PasswordChangeForm):
    user = get_object_or_404(User, pk=id)
    if request.method == "POST":
        form = password_change_form(user=user, data=request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, _("You have successfully changed your password. Please log in again."))
            return HttpResponseRedirect(reverse('home'))  # PROFILE
    else:
        form = password_change_form(user=user)
    return render(request=request, template_name=template_name, context={
        'user_this': user,
        'form': form,
    })


@login_required
def password_change_done_view(request, id, template_name='password_reset_send.html'):
    user_edit = get_object_or_404(User, pk=id)
    return render(request=request, template_name=template_name, context={'user_this': user_edit})
