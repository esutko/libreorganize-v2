import os
import uuid
import hashlib
import re

from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.db import connection, ProgrammingError

from apps.profiles.managers import ProfileManager, ProfileActiveManager


from timezone_field import TimeZoneField
import pytz
from datetime import datetime, timedelta


def get_timezone_choices():
    choices = []
    for tz in pytz.common_timezones:
        ofs = datetime.now(pytz.timezone(tz)).strftime("%z")
        choices.append((int(ofs), tz, "(GMT%s) %s" % (ofs, tz)))
    choices.sort()
    return [t[1:] for t in choices]


class Profile(models.Model):

    user = models.OneToOne(User, related_name="profile", on_delete=models.CASCADE)
    title = models.CharField(choices=settings.TITLE_CHOICES, required=False, label=_('Title'))
    date_of_birth = models.DateField(required=False, input_formats=['%m/%d/%Y'], label=_('Date of Birth'),
                                     help_text=_('Must be formatted as MM/DD/YYYY'))
    gender = models.CharField(choices=settings.GENDER_CHOICES, required=False, label=_('Gender'))
    company = models.CharField(max_length=50, required=False, label=_('Company / School / University'))
    phone = models.RegexField(regex=r'^[0-9]+$', max_length=10, required=False, label=_('Phone Number'),
                              help_text=_('Must not have any spaces, paranthesis, or dashes'))
    address1 = models.CharField(max_length=150, required=False, label=_('Address Line 1'),
                                help_text=_('Street address, P.O. box'))
    address2 = models.CharField(max_length=150, required=False, label=_('Address Line 2'),
                                help_text=_('Apartment, suite, unit, building, floor'))
    city = models.CharField(max_length=30, required=False, label=_('City'))
    state = models.CharField(max_length=30, required=False, label=_('State / Region / Province'))
    zipcode = models.CharField(max_length=10, required=False, label=_('ZIP / Postal Code'))
    country = models.CountryField(blank=True, blank_label='')

    time_zone = models.CharField(choices=TIMEZONES, default='US/Eastern', label=_('Time Zone'))
    language = models.CharField(_('Language'), choices=settings.LANGUAGES, default=settings.LANGUAGE_CODE)

    linkedin = models.URLField(_('LinkedIn'), blank=True, default='')
    facebook = models.URLField(_('Facebook'), blank=True, default='')
    twitter = models.URLField(_('Twitter'), blank=True, default='')
    instagram = models.URLField(_('Instagram'), blank=True, default='')
    youtube = models.URLField(_('YouTube'), blank=True, default='')

    objects = ProfileManager()
    actives = ProfileActiveManager()

    class Meta:
        abstract = True
        app_label = 'profiles'

    def get_absolute_url(self):
        from tendenci.apps.profiles.utils import clean_username
        cleaned_username = clean_username(self.user.username)
        if cleaned_username != self.user.username:
            self.user.username = cleaned_username
            self.user.save()
        return reverse('profile', args=[self.user.username])

    @property
    def is_active(self):
        return self._can_login()

    @property
    def is_member(self):
        if self.member_number and self._can_login():
            return True
        return False

    @property
    def is_superuser(self):
        return self.user.is_superuser

    @property
    def lang(self):
        if self.language not in [l[0] for l in settings.LANGUAGES]:
            self.language = 'en'
            self.save()
        return self.language

    def get_address(self):
        if self.address_type:
            return '%s (%s)' % (super(Profile, self).get_address(),
                                self.address_type)
        else:
            return super(Profile, self).get_address()

    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)

    def allow_search_users(self):
        """
        Check if this user can search users.
        """
        if self.is_superuser:
            return True
        return False

    def allow_view_by(self, user2_compare):
        """
        Check if `user2_compare` is allowed to view this user.
        """
        # user2_compare is superuser
        if user2_compare.is_superuser:
            return True

        # this user is user2_compare self
        if user2_compare == self.user:
            return True

        # user2_compare is creator or owner of this user
        if (self.creator and self.creator == user2_compare) or \
                (self.owner and self.owner == user2_compare):
            if self.status:
                return True

        # False for everythin else
        return False

    def allow_edit_by(self, user2_compare):
        """
        Check if `user2_compare` is allowed to edit this user.
        """
        if user2_compare.is_superuser:
            return True

        if user2_compare == self.user:
            return True

        if (self.creator and self.creator == user2_compare) or \
                (self.owner and self.owner == user2_compare):
            if self.status:
                return True

        return False

    def can_renew(self):
        """
        Looks at all memberships the user is actively associated
        with and returns whether the user is within a renewal period (boolean).
        """

        if not hasattr(self.user, 'memberships'):
            return False

        # look at active memberships

        active_memberships = self.user.memberships.filter(
            status=True, status_detail='active'
        )

        for membership in active_memberships:
            if membership.can_renew():
                return True

        return False

    @property
    def membership(self):
        [membership] = self.user.membershipdefault_set.exclude(
            status_detail='archive').order_by('-create_dt')[:1] or [None]
        return membership

    def refresh_member_number(self):
        """
        Adds or removes member number from profile.
        """
        membership = self.user.membershipdefault_set.first(
            status=True, status_detail__iexact='active'
        )

        self.member_number = u''
        if membership:
            if not membership.member_number:
                membership.set_member_number()
                membership.save()
            self.member_number = membership.member_number

        self.save()
        return self.member_number

    @classmethod
    def get_or_create_user(cls, **kwargs):
        """
        Return a user that's newly created or already existed.
        Return new or existing user.

        If username is passed.  It uses the username to return
        an existing user record or creates a new user record.

        If an email is passed.  It uses the email to return
        an existing user record or create a new user record.

        User is updated with first name, last name, and email
        address passed.

        If a password is passed; it is only used in order to
        create a new user account.
        """

        un = kwargs.get('username', u'')
        pw = kwargs.get('password', u'')
        fn = kwargs.get('first_name', u'')
        ln = kwargs.get('last_name', u'')
        em = kwargs.get('email', u'')

        user = None
        created = False

        if un:
            # created = False
            [user] = User.objects.filter(
                username=un)[:1] or [None]
        elif em:
            [user] = User.objects.filter(
                email=em).order_by('-pk')[:1] or [None]

        if not user:
            created = True
            user = User.objects.create_user(**{
                'username': un or Profile.spawn_username(fn[:1], ln),
                'email': em,
                'password': pw or uuid.uuid4().hex[:6],
            })

        user.first_name = fn
        user.last_name = ln
        user.email = em
        user.save()

        if created:
            profile = Profile.objects.create_profile(user)

        return user, created

    def roles(self):
        role_set = []

        if self.is_superuser:
            role_set.append('superuser')

        if self.is_staff:
            role_set.append('staff')

        if self.is_member:
            role_set.append('member')

        if self.is_active:
            role_set.append('user')

        return role_set or ['disabled']

    def highest_role(self):
        """
        The highest role will be returned.
        """
        roles = ['superuser', 'staff', 'member', 'user', 'disabled']
        for role in roles:
            if role in self.roles():
                return role

    def getMD5(self):
        m = hashlib.md5()
        m.update(self.user.email.encode())
        return m.hexdigest()
