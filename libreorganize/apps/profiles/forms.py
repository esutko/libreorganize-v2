import datetime
import re

from django import forms
from django.contrib import auth
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.forms.widgets import SelectDateWidget
from django.utils.safestring import mark_safe
from django.db.models import Q

from apps.base.fields import EmailVerificationField, CountrySelectField
from apps.base.utils import normalize_field_names
from apps.site_settings.utils import get_setting
from apps.user_groups.models import Group, GroupMembership
from apps.memberships.models import MembershipDefault
from apps.event_logs.models import EventLog
from apps.profiles.models import Profile, UserImport
from apps.profiles.utils import update_user
from apps.base.utils import get_languages_with_local_name
from apps.perms.utils import get_query_filters

attrs_dict = {'class': 'required' }
THIS_YEAR = datetime.date.today().year
# this is the list of apps whose permissions will be displayed on the permission edit page
APPS = ('profiles', 'user_groups', 'articles',
        'news', 'pages', 'jobs', 'locations',
        'stories', 'actions', 'photos', 'entities',
        'locations', 'files', 'directories', 'resumes',
        'memberships', 'corporate_memberships')


class ProfileSearchForm(forms.Form):
    SEARCH_CRITERIA_CHOICES = (
                        ('', _('SELECT ONE')),
                        ('first_name', _('First Name')),
                        ('last_name', _('Last Name')),
                        ('email', _('Email')),
                        ('username', _('Username')),
                        ('member_number', _('Member Number')),
                        ('company', _('Company')),
                        ('department', _('Department')),
                        ('position_title', _('Position Title')),
                        ('phone', _('Phone')),
                        ('city', _('City')),
                        ('region', _('Region')),
                        ('state', _('State')),
                        ('zipcode', _('Zip Code')),
                        ('country', _('Country')),
                        ('spouse', _('Spouse'))
                        )
    SEARCH_METHOD_CHOICES = (
                             ('starts_with', _('Starts With')),
                             ('contains', _('Contains')),
                             ('exact', _('Exact')),
                             )
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    email = forms.CharField(required=False)
    member_only = forms.BooleanField(label=_('Show Member Only'),
                                     widget=forms.CheckboxInput(),
                                     initial=True, required=False)
    membership_type = forms.IntegerField(required=False)
    group = forms.IntegerField(required=False)
    search_criteria = forms.ChoiceField(choices=SEARCH_CRITERIA_CHOICES,
                                        required=False)
    search_text = forms.CharField(max_length=100, required=False)
    search_method = forms.ChoiceField(choices=SEARCH_METHOD_CHOICES,
                                        required=False)

    def __init__(self, *args, **kwargs):
        mts = kwargs.pop('mts')
        self.user = kwargs.pop('user')
        super(ProfileSearchForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs.update({'placeholder': _('Exact Match Search')})
        self.fields['last_name'].widget.attrs.update({'placeholder': _('Exact Match Search')})
        self.fields['email'].widget.attrs.update({'placeholder': _('Exact Match Search')})

        if not mts:
            del self.fields['membership_type']
            del self.fields['member_only']
        else:
            choices = [(0, _('SELECT ONE'))]
            choices += [(mt.id, mt.name) for mt in mts]
            self.fields['membership_type'].widget = forms.widgets.Select(
                                    choices=choices)

        # group choices
        filters = get_query_filters(self.user, 'user_groups.view_group', **{'perms_field': False})
        group_choices = [(0, _('SELECT ONE'))] + list(Group.objects.filter(
                            status=True, status_detail="active"
                             ).filter(filters).distinct().order_by('name'
                            ).values_list('pk', 'name'))
        self.fields['group'].widget = forms.widgets.Select(
                                    choices=group_choices)



class ValidatingPasswordChangeForm(auth.forms.PasswordChangeForm):
    def __init__(self, user, *args, **kwargs):
        super(ValidatingPasswordChangeForm, self).__init__(user, *args, **kwargs)

        self.fields['old_password'].widget = forms.PasswordInput(attrs={'class': 'form-control'})
        self.fields['new_password1'].widget = forms.PasswordInput(attrs={'class': 'form-control'})
        self.fields['new_password2'].widget = forms.PasswordInput(attrs={'class': 'form-control'})

    def clean_new_password1(self):
        password1 = self.cleaned_data.get('new_password1')
        password_regex = get_setting('module', 'users', 'password_requirements_regex')
        password_requirements = get_setting('module', 'users', 'password_text')
        if password_regex:
            # At least MIN_LENGTH long
            # r'^(?=.{8,})(?=.*[0-9=]).*$'
            if not re.match(password_regex, password1):
                raise forms.ValidationError(mark_safe("The new password does not meet the requirements </li><li>%s" % password_requirements))

        return password1


class UserMembershipForm(models.Model):
    join_dt = forms.SplitDateTimeField(label=_('Subscribe Date/Time'),
        initial=datetime.datetime.now())
    expire_dt = forms.SplitDateTimeField(label=_('Expire Date/Time'), required=False)
    status_detail = forms.ChoiceField(
        choices=(('active',_('Active')),('inactive',_('Inactive')), ('pending',_('Pending')),))

    class Meta:
        model = MembershipDefault
        fields = (
            'member_number',
            'membership_type',
            'join_dt',
            'expire_dt',
            'allow_anonymous_view',
            'user_perms',
            'member_perms',
            'group_perms',
            'status',
            'status_detail',
        )

        fieldsets = [
            (_('Membership Information'), {
                'fields': [
                    'member_number',
                    'membership_type',
                    'join_dt',
                    'expire_dt',
                ],
                'legend': ''
                }),
            (_('Permissions'), {
                'fields': [
                    'allow_anonymous_view',
                    'user_perms',
                    'member_perms',
                    'group_perms',
                ],
                'classes': ['permissions'],
            }),
            (_('Administrator Only'), {
                'fields': [
                    'status',
                    'status_detail'],
                'classes': ['admin-only'],
            })]

    def __init__(self, *args, **kwargs):
        super(UserMembershipForm, self).__init__(*args, **kwargs)


class ProfileMergeForm(forms.Form):
    user_list = forms.ModelMultipleChoiceField(queryset=Profile.objects.none(),
                    label=_("Choose the users to merge"),
                    widget=forms.CheckboxSelectMultiple())

    master_record = forms.ModelChoiceField(queryset=Profile.objects.none(),
                        empty_label=None,
                        label=_("Choose the master record of the users you are merging above"),
                        widget=forms.RadioSelect())

    def __init__(self, *args, **kwargs):
        choices = kwargs.pop('list', None)
        super(ProfileMergeForm, self).__init__(*args, **kwargs)

        queryset = Profile.objects.filter(user__in=choices).order_by('-user__last_login')

        self.fields["master_record"].queryset = queryset
        self.fields["user_list"].queryset = queryset

        if queryset.count() == 2:
            self.fields['user_list'].initial = queryset


class UserUploadForm(forms.ModelForm):
    KEY_CHOICES = (('email', _('Email')),
               ('first_name,last_name,email', _('First Name and Last Name and Email')),
               ('first_name,last_name,phone', _('First Name and Last Name and Phone')),
               ('first_name,last_name,company', _('First Name and Last Name and Company')),
               ('username', 'Username'),)

    interactive = forms.BooleanField(widget=forms.RadioSelect(
                                    choices=UserImport.INTERACTIVE_CHOICES),
                                    initial=False, required=False)
    key = forms.ChoiceField(label="Key",
                            choices=KEY_CHOICES)
    group_id = forms.ChoiceField(label=_("Add Users to Group"),
                            required=False)
    clear_group_membership = forms.BooleanField(initial=False, required=False)

    class Meta:
        model = UserImport
        fields = (
                'key',
                'override',
                'interactive',
                'group_id',
                'clear_group_membership',
                'upload_file',
                  )

    def __init__(self, *args, **kwargs):
        super(UserUploadForm, self).__init__(*args, **kwargs)
        self.fields['key'].initial = 'email'
        # move the choices down here to fix the error
        #  django.db.utils.ProgrammingError: relation "user_groups_group" does not exist
        GROUP_CHOICES = [(0, _('Select One'))] + [(group.id, group.name) for group in
                     Group.objects.filter(status=True, status_detail='active'
                                          ).exclude(type='membership')]
        self.fields['group_id'].choices = GROUP_CHOICES

    def clean_upload_file(self):
        key = self.cleaned_data['key']
        upload_file = self.cleaned_data['upload_file']
        if not key:
            raise forms.ValidationError(_('Please specify the key to identify duplicates'))

        file_content = upload_file.read().decode('utf-8') # decode from bytes to string
        upload_file.seek(0)
        header_line_index = file_content.find('\n')
        header_list = ((file_content[:header_line_index]
                            ).strip('\r')).split(',')
        header_list = normalize_field_names(header_list)
        key_list = []
        for key in key.split(','):
            key_list.append(key)
        missing_columns = []
        for item in key_list:
            if item not in header_list:
                missing_columns.append(item)
        if missing_columns:
            raise forms.ValidationError(_(
                        """
                        'Field(s) %(fields)s used to identify the duplicates
                        should be included in the .csv file.'
                        """ % {'fields' : ', '.join(missing_columns)}))

        return upload_file


class ActivateForm(forms.Form):
    email = forms.CharField(max_length=75)
    username = forms.RegexField(regex=r'^[\w.@+-]+$',
                                max_length=30, required=False)
