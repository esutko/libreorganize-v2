from django.conf.urls import url
from . import views
from django.views.generic import RedirectView

app_name = 'apps.profiles'

urlpatterns = [
    url(r'^%s/$' % urlpath, views.index, name="profile.index"),
    url(r'^%s/admins/$' % urlpath, views.admin_list, name="profile.admins"),
    url(r'^%s/search/$' % urlpath, views.search, name="profile.search"),
    url(r'^%s/add/$' % urlpath, views.add, name="profile.add"),
    url(r'^%s/edit/(?P<id>\d+)/$' % urlpath, views.edit, name="profile.edit"),
    url(r'^%s/similar/$' % urlpath, views.similar_profiles, name="profile.similar"),
    url(r'^%s/merge/(?P<sid>\d+)/$' %
        urlpath, views.merge_profiles, name="profile.merge_view"),
    url(r'^%s/edit_perms/(?P<id>\d+)/$' %
        urlpath, views.edit_user_perms, name="profile.edit_perms"),
    url(r'^%s/delete/(?P<id>\d+)/$' %
        urlpath, views.delete, name="profile.delete"),

    # activate inactive user account
    url(r'^%s/activate-email/$' %
        urlpath, views.activate_email, name="profile.activate_email"),

    url(r'^%s/(?P<username>[+-.\w\d@\s]+)/$' %
        urlpath, views.index, name='profile'),
    url(r'^%s/(?P<username>[+-.\w\d@\s]+)/groups/edit/$' %
        urlpath, views.user_groups_edit, name='profile.edit_groups'),
    url(r'^%s/(?P<username>[+-.\w\d@\s]+)/education/edit/$' %
        urlpath, views.user_education_edit, name='profile.edit_education'),
    url(r'^%s/(?P<username>[+-.\w\d@\s]+)/groups/(?P<membership_id>\d+)/edit/$' %
        urlpath, views.user_role_edit, name='profile.edit_role'),
    url(r'^%s/(?P<username>[+-.\w\d@\s]+)/memberships/add/$' %
        urlpath, views.user_membership_add, name='profile.add_membership'),
]

urlpatterns += [
    # Special redirect for user.get_absolute_url
    url(r'^users/(?P<username>[+-.\w\d@\s]+)/$', RedirectView.as_view(
        url='/%s/%s/' % (urlpath, '%(username)s'), permanent=True)),
]
